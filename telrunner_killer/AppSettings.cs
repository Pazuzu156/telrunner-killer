﻿using Newtonsoft.Json;
using System;
using System.IO;

namespace telrunner_killer
{
    public class AppSettings
    {
        private readonly string mSettingsFile;
        private static AppSettings mInstance;
        public SettingsData Config { get; private set; }

        public static AppSettings GetInstance()
        {
            if (mInstance == null) {
                mInstance = new AppSettings();
            }

            return mInstance;
        }

        private AppSettings()
        {
            var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            mSettingsFile = $"{appData}\\telrunner_killer_settings.json";

            LoadSettings();
        }

        private void LoadSettings()
        {
            if (!File.Exists(mSettingsFile)) {
                CreateSettings();
            }

            using (var reader = new StreamReader(File.OpenRead(mSettingsFile))) {
                Config = JsonConvert.DeserializeObject<SettingsData>(reader.ReadToEnd());
            }
        }

        private void CreateSettings()
        {
            var handle = File.Create(mSettingsFile);
            handle.Close();
            Save(true);
        }

        public void Save(bool create = false)
        {
            File.Delete(mSettingsFile);
            var handle = File.Create(mSettingsFile);
            handle.Close();

            using (var writer = new StreamWriter(File.OpenWrite(mSettingsFile))) {
                if (create) {
                    Config = new SettingsData() {
                        ShowBalloonOnTermination = true
                    };
                }

                var jsonString = JsonConvert.SerializeObject(Config, Formatting.Indented);
                writer.Write(jsonString);
            }
        }
    }

    public class SettingsData
    {
        public bool ShowBalloonOnTermination { get; set; }
    }
}
