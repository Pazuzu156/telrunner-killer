﻿using System;
using System.Windows.Forms;

namespace telrunner_killer
{
    public class Notification
    {
        private AppSettings mSettings;
        private UpdateChecker mUpdateChecker;

        private NotifyIcon mIcon;
        private ContextMenu mIconMenu;

        private MenuItem mItemInfo;
        private MenuItem mItemAbout;
        private MenuItem mItemTerminationCount;
        private MenuItem mCheckItemPopupOnTerminate;
        private MenuItem mItemPauseResume;
        private MenuItem mItemCheckForUpdates;
        private MenuItem mItemExit;

        private TelrunnerListener mListener;
        private int mProcessTerminationCount = 0;
        private bool mShowBaloonTipOnProcessTermination;
        private bool mIsPaused = false;

        public Notification()
        {
            mSettings = AppSettings.GetInstance();
            mUpdateChecker = new UpdateChecker();

            mShowBaloonTipOnProcessTermination = mSettings.Config.ShowBalloonOnTermination;

            GenerateNotifyIcon();
        }

        private void GenerateNotifyIcon()
        {
            mIcon = new NotifyIcon();
            mIconMenu = new ContextMenu();

            mItemInfo = new MenuItem();
            mItemAbout = new MenuItem();
            mItemTerminationCount = new MenuItem();
            mCheckItemPopupOnTerminate = new MenuItem();
            mItemPauseResume = new MenuItem();
            mItemCheckForUpdates = new MenuItem();
            mItemExit = new MenuItem();

            mIconMenu.MenuItems.Add(mItemInfo);
            mIconMenu.MenuItems.Add(mItemAbout);
            mIconMenu.MenuItems.Add("-");
            mIconMenu.MenuItems.Add(mItemTerminationCount);
            mIconMenu.MenuItems.Add(mCheckItemPopupOnTerminate);
            mIconMenu.MenuItems.Add("-");
            mIconMenu.MenuItems.Add(mItemPauseResume);
            mIconMenu.MenuItems.Add(mItemCheckForUpdates);
            mIconMenu.MenuItems.Add(mItemExit);

            mItemInfo.Text = $"Telrunner Killer v{App.AppVersion}";
            mItemInfo.Enabled = false;

            mItemAbout.Text = "About Telrunner Killer";
            mItemAbout.Click += M_ItemAbout_Click;

            mItemTerminationCount.Text = "Process termination count: 0";
            mItemTerminationCount.Enabled = false;

            mCheckItemPopupOnTerminate.Text = "Show Notification on Termination";
            mCheckItemPopupOnTerminate.Checked = mShowBaloonTipOnProcessTermination;
            mCheckItemPopupOnTerminate.Click += M_CheckItemPopupOnTerminate_Click;

            mItemPauseResume.Text = "Pause Listener";
            mItemPauseResume.Click += M_ItemPauseResume_Click;

            mItemCheckForUpdates.Text = "Check for Updates";
            mItemCheckForUpdates.Click += M_ItemCheckForUpdates_Click;

            mItemExit.Text = "Exit";
            mItemExit.Click += M_ItemExit_Clicked;

            mIcon.ContextMenu = mIconMenu;

            mIcon.Icon = Properties.Resources.knife;
            mIcon.Visible = true;
            mIcon.Text = "Telrunner Killer";
            mIcon.MouseClick += M_Icon_MouseClick;
            mIcon.MouseDoubleClick += M_Icon_MouseDoubleClick;

            mListener = new TelrunnerListener();
            mListener.ProcessTerminated += M_Listener_ProcessTerminated;
        }

        private void M_ItemPauseResume_Click(object sender, EventArgs e)
        {
            if (mIsPaused) {
                mItemPauseResume.Text = "Pause";
                mIsPaused = false;
                mListener.Resume();
                mIcon.ShowBalloonTip(1500, "Listener Resumed", "The process listener has been resumed.", ToolTipIcon.Info);
            } else {
                mItemPauseResume.Text = "Resume";
                mIsPaused = true;
                mListener.Pause();
                mIcon.ShowBalloonTip(1500, "Listener Paused", "The process listener has been paused. Click \"Resume\" to start the listener back up.", ToolTipIcon.Info);
            }
        }

        private void M_Icon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            mIcon.ShowBalloonTip(0, "Termination Count", $"CompatTelRunner has been killed {mProcessTerminationCount} time" +
                ((mProcessTerminationCount == 1) ? "" : "s") + ".", ToolTipIcon.Info);
        }

        private void M_Icon_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle) {
                mListener.Dispose();
            }
        }

        private void M_ItemCheckForUpdates_Click(object sender, EventArgs e) => mUpdateChecker.CheckForUpdates(true);

        private void M_CheckItemPopupOnTerminate_Click(object sender, EventArgs e)
        {
            mShowBaloonTipOnProcessTermination = !mShowBaloonTipOnProcessTermination;
            mCheckItemPopupOnTerminate.Checked = mShowBaloonTipOnProcessTermination;

            mSettings.Config.ShowBalloonOnTermination = mShowBaloonTipOnProcessTermination;
            mSettings.Save();
        }

        private void M_Listener_ProcessTerminated(object sender, EventArgs args)
        {
            mProcessTerminationCount++;

            if (mShowBaloonTipOnProcessTermination) {
                mIcon.ShowBalloonTip(0, "CompatTelRunner Terminated", $"Process eleminated {mProcessTerminationCount} time" +
                    ((mProcessTerminationCount == 1) ? "" : "s" + "."), ToolTipIcon.Info);
            }

            mItemTerminationCount.Text = $"Process termination count: {mProcessTerminationCount}";
        }

        private void M_ItemAbout_Click(object sender, EventArgs e)
        {
            var about = new AboutWindow();
            about.Show();
        }

        private void M_ItemExit_Clicked(object sender, EventArgs e) => mListener.Dispose();
    }
}
