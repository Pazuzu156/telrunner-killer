﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Windows;
using System.Diagnostics;

namespace telrunner_killer
{
    public class UpdateChecker
    {
        private StatusCheck mStatusCheck;

        public UpdateChecker()
        {
            mStatusCheck = new StatusCheck();
        }

        public void CheckForUpdates(bool showErrorMessage = false)
        {
            if (mStatusCheck.IsConnected) {
                try {
                    var onlineData = LoadOnlineVersionData();

                    if (onlineData.Version > App.AppVersion) {
                        var res = MessageBox.Show("A new version of Telrunner Killer is available. Would you like to download the update now?", "Update Available", MessageBoxButton.YesNo, MessageBoxImage.Question);

                        if (res == MessageBoxResult.Yes) {
                            Process.Start($"https://bitbucket.org/pazuzu156/telrunner-killer/downloads/telrunner_killer-{onlineData.Version}.zip");
                        }
                    } else {
                        if (showErrorMessage) {
                            MessageBox.Show("You are already up-to-date!", "Update Check", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                } catch {
                    if (showErrorMessage) {
                        MessageBox.Show("Couldn't connect to the update server. Please try again later", "Update Check", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private OnlineVersionData LoadOnlineVersionData()
        {
            WebRequest req = WebRequest.Create("https://cdn.kalebklein.com/updates/telrunner_killer.json");
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();

            using (var reader = new StreamReader(res.GetResponseStream())) {
                return JsonConvert.DeserializeObject<OnlineVersionData>(reader.ReadToEnd());
            }
        }
    }

    public class OnlineVersionData
    {
        public Version Version { get; set; }
    }
}
