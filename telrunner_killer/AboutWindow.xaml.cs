﻿using System.Diagnostics;
using System.Drawing;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace telrunner_killer
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();

            Icon icon = Properties.Resources.knife;
            ImageSource img = Imaging.CreateBitmapSourceFromHIcon(
                icon.Handle,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            Icon = img;
        }

        private void BB_Link_MouseDown(object sender, RoutedEventArgs e) => Process.Start("https://bitbucket.org/pazuzu156/telrunner-killer");

        private void DryIcons_Link_MouseDown(object sender, RoutedEventArgs e) => Process.Start("https://dryicons.com/icon/bloody-knife-icon-11429");

        private void Button_Click(object sender, RoutedEventArgs e) => Visibility = Visibility.Hidden;

        private void EscapeCommand_Executed(object sender, System.Windows.Input.ExecutedRoutedEventArgs e) => Visibility = Visibility.Hidden;

        private void EscapeCommand_CanExecute(object sender, System.Windows.Input.CanExecuteRoutedEventArgs e) => e.CanExecute = true;
    }
}
