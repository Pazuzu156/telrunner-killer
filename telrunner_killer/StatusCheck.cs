﻿using System;
using System.Net;

namespace telrunner_killer
{
    class StatusCheck
    {
        /// <summary>
        /// Holds whether you're connected to the internet or not.
        /// </summary>
        public bool IsConnected { get; private set; }

        private readonly string mUrl;

        /// <summary>
        /// Creates a new status check and performs the test.
        /// </summary>
        /// <param name="url"></param>
        public StatusCheck(string url = "https://google.com")
        {
            mUrl = url;
            TestConnection();
        }

        /// <summary>
        /// Creates a new status check and performs the test.
        /// </summary>
        /// <param name="url"></param>
        public StatusCheck(Uri url)
        {
            mUrl = url.ToString();
            TestConnection();
        }

        /// <summary>
        /// Performs a connection test.
        /// </summary>
        private void TestConnection()
        {
            WebRequest req = WebRequest.Create(this.mUrl);
            req.Timeout = 1000;

            try {
                HttpWebResponse res = (HttpWebResponse)req.GetResponse();

                if (res.StatusCode == HttpStatusCode.OK) {
                    IsConnected = true;
                }
            } catch {
                IsConnected = false;
            }
        }
    }
}
