﻿using System.Windows.Input;

namespace telrunner_killer
{
    public static class ActionCommands
    {
        static ActionCommands()
        {
            Escape = new RoutedUICommand("Close Window", "CloseWindow", typeof(ActionCommands),
                new InputGestureCollection {
                    new KeyGesture(Key.Escape, ModifierKeys.None, "ESC")
                });
        }

        public static RoutedUICommand Escape { get; private set; }
    }
}
