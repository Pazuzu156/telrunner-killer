# Telrunner Killer
Telrunner Killer is a super small/simple app that listens for the CompatTelRunner.exe process (Microsoft Windows Compatibility Telemetry) and kills it. This is basically malware that you cannot remove from your system without causing instability. It also collects data, but what data? And sending it to who? I don't care, I don't want it running.

Not only does it collect data, but holy Jesus does it eat up CPU resources. This app hardly uses any, and will kill the process on site.

## Running
You need .NET 4.5. Google a redistributable for this and you should be fine. Download from the downloads section of the repository, and extract `Telrunner Killer.exe` and `Newtonsoft.Json.dll` anywhere you want, and run it. It'll run in the background, and add a notification icon to your taskbar tray. Interact with the app there.

## Config
Simple configuration, like 1 setting. Still, I needed to save that somewhere... Look in `C:\Users\<YOUR_USER>\AppData\Roaming\telrunner_killer_settings.json`. Super simple JSON file you can edit/delete/whatever. This is the only file Telrunner Killer places on your system. Delete it if you don't want to use the app anymore. No installers/uninstallers with complicated registry entries and shit-tons of files. I hate apps that do that...

## Building
There's a build script (Batch file) included. As long as you have Visual Studio 2017 Community Edition installed and at least .NET 4.5, it should build with ease.

## Uninstall
Delete `Telrunner Killer.exe` and `Newtonsoft.Json.dll` and the config file that was mentioned in [Config](#config) and you're done!
