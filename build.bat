@echo off

set /p BUILDSEL="Select build configuration: (D - Debug // R - Release) "

if "%BUILDSEL%"=="D" (
    set BUILD="Debug"
) else if "%BUILDSEL%"=="d" (
    set BUILD="Debug"
) else if "%BUILDSEL%"=="R" (
    set BUILD="Release"
) else if "%BUILDSEL%"=="r" (
    set BUILD="Release"
) else (
    set BUILD="Release"
)

echo Setting up environment...
set CWD=%~dp0
set MSFW=C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin
PATH=%PATH%;%MSFW%;
set CFLAGS=/p:Configuration=%BUILD%;AllowUnsafeBlocks=true /p:CLSCompliant=False
set LDFLAGS=/p:TargetFrameworkVersion=v4.5 /p:Platform="Any Cpu" /p:OutputPath="%CWD%\bin"

echo Downloading required packages...
utils\nuget.exe install telrunner_killer\packages.config -o packages

echo Compiling application...
msbuild telrunner_killer.sln %CFLAGS% %LDFLAGS%
if erorlevel 1 goto error

goto done

:error
echo An error occurred while compiling. Check the output for errors
pause
exit

:done
echo Complete!
pause
