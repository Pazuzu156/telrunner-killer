﻿using System;
using System.Threading;
using System.Diagnostics;

namespace telrunner_killer
{
    public class TelrunnerListener
    {
        private Thread mListener;
        private ManualResetEvent mPauseEvent;
        private readonly bool mTest = false;
        private bool mRunning;

        public TelrunnerListener()
        {
            mPauseEvent = new ManualResetEvent(true);
            CreateThread();
        }

        public void Pause()
        {
            mPauseEvent.Reset();
        }

        public void Resume()
        {
            mPauseEvent.Set();
        }

        public void Dispose()
        {
            mRunning = false;
            mListener.Join();
            
            while (true) {
                if (mListener.ThreadState == System.Threading.ThreadState.Stopped) {
                    mListener = null;

                    break;
                }
            }

            Terminate();
        }

        private void CreateThread()
        {
            mRunning = true;
            mListener = new Thread(Listen);
            mListener.Start();
        }

        private void Listen()
        {
            while (mRunning) {
                mPauseEvent.WaitOne(Timeout.Infinite);

                var runnerName = "CompatTelRunner" + ((mTest) ? "_Test" : "");
                Process[] pList = Process.GetProcessesByName(runnerName);

                foreach (Process process in pList) {
                    try {
                        process.Kill();
                        OnProcessTerminated();
                    } catch { }
                }

                Thread.Sleep(1500);
            }
        }

        private void Terminate()
        {
            App.Current.Dispatcher.Invoke(() => {
                App.Current.Shutdown();
            });
        }

        public delegate void ProcessTerminatedEventHandler(object sender, EventArgs args);
        public event ProcessTerminatedEventHandler ProcessTerminated;
        protected virtual void OnProcessTerminated() => ProcessTerminated?.Invoke(this, EventArgs.Empty);
    }
}
