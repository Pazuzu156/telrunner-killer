@echo off

echo Setting up environment...
set CWD=%~dp0

echo Cleaning up binaries
if exist bin rmdir /s /q bin
if exist packages rmdir /s /q packages

cd "%CWD%\telrunner_killer"

if exist bin rmdir /s /q bin
if exist obj rmdir /s /q obj

cd "%CWD%"

echo Complete!
pause
