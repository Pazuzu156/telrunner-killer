﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using System.Windows;

namespace telrunner_killer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Mutex mMutex;

        public static Version AppVersion { get; private set; }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var mutexName = $"Local\\{{{assembly.GetType().GUID}}}{{{assembly.GetName().Name}}}";
            mMutex = new Mutex(true, mutexName, out bool mutexCreated);

            if (!mutexCreated) {
                mMutex = null;
                MessageBox.Show("You can only have one instance of Telrunner Killer open at a time!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                Current.Shutdown();

                return;
            }

            AppVersion = GetVersion();

            var uc = new UpdateChecker();
            uc.CheckForUpdates();


            new Notification();
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            if (mMutex == null) {
                return;
            }

            mMutex.ReleaseMutex();
            mMutex.Close();
            mMutex = null;
        }

        private Version GetVersion() => Assembly.GetExecutingAssembly().GetName().Version;
    }
}
